
// Welcome.js is the child component
// props from Home.js (parent component)
export default function Welcome (props) {
	return (
	<>
		<h1>Hello, {props.name}. Age: {props.age} </h1>
		<p>Welcome to our Course Booking!</p>
	</>
		)
}