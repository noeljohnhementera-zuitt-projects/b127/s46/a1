import { useState } from 'react'; // this function acts as a hook ng State
// Bootstrap
import { Row, Col, Card, Button, Alert} from 'react-bootstrap';
import PropTypes from 'prop-types'; // Validator kung tama ang pinapasa natin na data
/*export default function CourseCard (props) {
	// Checks to see if the data was successfully passed
	console.log(props)
	console.log(typeof props)
	return (
			<Row>
				<Col>
					<Card>
						<Card.Body>
							<Card.Title><h2>{props.courseProp.name}</h2></Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{props.courseProp.description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {props.courseProp.price}</Card.Text>
							<Button variant='primary'>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}*/

// Destructuring Example 2

export default function CourseCard ({courseProp}) {
	//Props in React.js, short for properties, React.js are the information that a component receives, 
		// usually from a parent component.
	// Checks to see if the data was successfully passed
	// console.log(props)
	// console.log(typeof props)

	// Deconstruct the course properties into their own variables(destructuring)
	const { name, description, price } = courseProp;

	// use the state hook for this component to be able to store its state
	// States are use to keep track of information related to individual component
		// Those that requirerapid changes. Analogy is stop, wait, go
	// Syntax
		// const [getter, setter] = useState(initialGetterValue)
		// getter = initial / default value
		// setter = updated value
	/*const [count, setCount] = useState(0)

	console.log(useState(0))

	function enroll () {
		if(count >= 0 && count <=9){
			setCount(count + 1)
			console.log('Enrolees: ' + count);	
		}else{
			alert("No more seats Available!")
		}
	}*/
	// stop Enrolees: 8 wait (2) [0, ƒ] and go Enrolees: 9 sa front-end
	const [count, setCount] = useState(0)
	const [seat, seatCount] = useState(10)

	console.log(useState(0))

	function enroll () {
		if(count >= 0 && count <=9){
			setCount(count + 1)
			console.log('Enrolees: ' + count)
			seatCount(seat - 1)
			console.log('Seats: ' + seat)
		}else{
			alert("No more seats Available!")
		}
	}

	return (
			<Row>
				<Col>
					<Card>
						<Card.Body>
							<Card.Title><h2>{name}</h2></Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Text>Enrolees: {count}</Card.Text>
							{<Card.Text>Seats: {seat}</Card.Text>}
							<Button variant='primary' onClick={enroll}>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}

// Check if the CourseCard Component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool
	// normally used to help developers ensure the correct information is passed from
	// one component to the next
CourseCard.propTypes = { // as is si propTypes
	// The "shape" method is used to check if a prop object conforms to a specific 'shape' or if it is really a string, number, etc
	courseProp: PropTypes.shape({
		// define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

/*// Destructuring Example 3

export default function CourseCard ({courseProp}) {
	// Checks to see if the data was successfully passed
	// console.log(props)
	// console.log(typeof props)
	return (
			<Row>
				<Col>
					<Card>
						<Card.Body>
							<Card.Title><h2>{courseProp.name}</h2></Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{courseProp.description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {courseProp.price}</Card.Text>
							<Button variant='primary'>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}*/