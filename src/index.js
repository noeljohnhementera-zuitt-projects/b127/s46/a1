
// Listed lahat ng dependencies sa node_modules

// index.js is the main entry point
// Single Page Application (SPA) Maraming pages pero single page lang ang nag-r-run (index.js) 
import React from 'react';			// dependency sya. It automatically has this feature upon installing
import ReactDOM from 'react-dom';	// dependency sya. It automatically has this feature upon installing
import App from './App';			// Dito naka import ang App.js

// Components
//import AppNavbar from './AppNavbar';

// Import the bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css"

// ONLY ONE component is rendered here
ReactDOM.render(					// Strictmode gives more information about the error sa loob ng component
  <React.StrictMode>				
    <App />					
  </React.StrictMode>,				
  document.getElementById('root')	// index.html sa public folder
);

/*const name = "John Smith";
const user = {
	firstName: "Jasmine",
	lastName: "Smith"
}

function formatName(user) {
	return user.firstName + ' ' + user.lastName;
}
const element = <h1>Hello, {formatName(user)} </h1>

ReactDOM.render(element, document.getElementById('root'));
// ReactDOM.render is used to inject react js components and index.html*/